/**
 * console.log wrapper that only outputs if DEBUG is set to true in env.
 * @param content - params to output.
 */
export const debug = (...content): void => {
  if (process.env.DEBUG) {
    console.log(...content);
  }
};
