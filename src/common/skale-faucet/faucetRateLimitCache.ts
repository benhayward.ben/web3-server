import NodeCache from 'node-cache';

const SINGLE_WEEK_S = 604800;
const SINGLE_DAY_S = 604800 / 7;

/**
 * Rate limit cache for SKALE faucet
 */
export class SkaleFaucetRateLimitCache {
  // instance of class.
  private static _instance: SkaleFaucetRateLimitCache;

  // node cache instance.
  private cache: NodeCache;

  private constructor() {
    this.cache = new NodeCache({
      stdTTL: SINGLE_WEEK_S,
      deleteOnExpire: true,
      checkperiod: SINGLE_DAY_S
    });
  }

  /**
   * Gets instance of class, or creates a new one if one does not already exist.
   * @returns { SkaleFaucetRateLimitCache }
   */
  static getInstance(): SkaleFaucetRateLimitCache {
    if (this._instance) {
      return this._instance;
    }

    this._instance = new SkaleFaucetRateLimitCache();
    return this._instance;
  }

  /**
   * Get from cache.
   * @param { string } address - address (key) to get from cache.
   * @returns { boolean } true if key exists.
   */
  public get(address: string): boolean {
    return this.cache.get(address);
  }

  /**
   * Sets a key as an address with a default value of true.
   * @param { string } address - address (key) to set.
   * @param { number } ttl - time to live in seconds - defaults to 1 week.
   */
  public set(address: string, ttl: number = SINGLE_WEEK_S): void {
    this.cache.set(address, true, ttl);
  }

  /**
   * Deletes a key from the cache.
   * @param { string } address - address used as the key to delete.
   * @returns { void }
   */
  public del(address: string): void {
    this.cache.del(address);
  }
}
