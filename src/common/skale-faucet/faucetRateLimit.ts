import express from 'express';
import { SkaleFaucetRateLimitCache } from './faucetRateLimitCache';

const cache: SkaleFaucetRateLimitCache =
  SkaleFaucetRateLimitCache.getInstance();

/**
 * Skale Faucet Rate Limit Middleware.
 */
export const skaleFaucetRateLimit = function (): (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => Promise<void> {
  return async (req, res, next) => {
    if (process.env.BYPASS_RATE_LIMITS) {
      return next();
    }

    const requester = req.body.requester;

    if (!req.body.requester) {
      next(
        res.json({
          status: 400,
          message: 'Missing Requester'
        })
      );
    }

    if (cache.get(requester)) {
      res.json({
        status: 429,
        message: 'Too Many Requests'
      });
    } else {
      cache.set(requester);
      next();
    }
  };
};
