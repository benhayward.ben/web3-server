import express from 'express';
import * as ethers from 'ethers';
import { grantAccess } from '../../common/authenticate';
import { WalletManager } from '../../common/walletManager';
import { SupportedNetwork } from '../../common/types';
import { ProviderManager } from '../../common/providerManager';
import {
  communityPoolAbi,
  communityPoolAddress
} from '../../abi/communityPool';
import { debug } from '../../common/utils';

const router: any = express.Router();
const walletManager: WalletManager = WalletManager.getInstance();
const providerManager: ProviderManager = ProviderManager.getInstance();

// Authentication middleware
router.use('/', grantAccess('general'));

/**
 * @swagger
 * /skale/communityPool/canExit:
 *   post:
 *     summary: Checks whether community pool balance allows a user to exit from the SKALE chain.
 *     tags: [skale]
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: requester
 *         in: path
 *         required: true
 *         type: string
 *         description: ETH address to check.
 *     responses:
 *       200:
 *         description: Success, returns tx hash.
 *       400:
 *         description: Requester is not defined.
 *       401:
 *         description: Missing authentication headers.
 *       500:
 *         description: Internal server error.
 */
router.get('/canExit', async (req: express.Request, res: express.Response) => {
  const network = (req.header('X-ETH-NETWORK') ??
    'mainnet') as SupportedNetwork;

  const requester = req.body.requester;

  if (!requester) {
    return res.json({
      status: 400,
      message: 'Requester is not defined.'
    });
  }

  const sChainHash = process.env.SKALE_CHAIN_HASH;

  const provider = providerManager.getProvider(network);

  const walletSigner = walletManager
    .getWallet('general', network)
    .connect(provider);

  const communityPoolContact = new ethers.Contract(
    communityPoolAddress,
    communityPoolAbi,
    walletSigner
  );

  try {
    const result = await communityPoolContact.checkUserBalance(
      sChainHash,
      requester
    );

    debug(`Checking canWithdraw for ${requester}:`, result);

    return res.json({
      status: 200,
      data: result
    });
  } catch (e) {
    console.error(e);
    return res.json({
      status: 500,
      message: 'An unknown error has occurred.'
    });
  }
});

export { router };
