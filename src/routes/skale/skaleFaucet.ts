import express from 'express';
import * as ethers from 'ethers';
import { grantAccess } from '../../common/authenticate';
import { skaleFaucetRateLimit } from '../../common/skale-faucet/faucetRateLimit';
import { WalletManager } from '../../common/walletManager';
import { SupportedNetwork } from '../../common/types';
import { ProviderManager } from '../../common/providerManager';
import { debug } from 'console';
import { SkaleFaucetRateLimitCache } from '../../common/skale-faucet/faucetRateLimitCache';
import {
  communityPoolAbi,
  communityPoolAddress
} from '../../abi/communityPool';

const router: any = express.Router();
const walletManager: WalletManager = WalletManager.getInstance();
const providerManager: ProviderManager = ProviderManager.getInstance();
const cache: SkaleFaucetRateLimitCache =
  SkaleFaucetRateLimitCache.getInstance();

// Authentication middleware
router.use('/', grantAccess('skale'));

// Rate limit middleware (once per week)
router.use('/', skaleFaucetRateLimit());

/**
 * @swagger
 * /skale/faucet:
 *   post:
 *     summary: Request skETH from SKALE faucet.
 *     tags: [skale]
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: requester
 *         in: path
 *         required: true
 *         type: string
 *         description: ETH address requesting funds.
 *     responses:
 *       200:
 *         description: Success, returns tx hash.
 *       400:
 *         description: Invalid request or balance too high.
 *       401:
 *         description: Missing authentication headers.
 *       500:
 *         description: Internal server error or tx failed.
 */
router.post('/', async (req: express.Request, res: express.Response) => {
  const requester = req.body.requester;

  try {
    const network: any = (req.header('X-ETH-NETWORK') ??
      'skale') as SupportedNetwork;

    if (!requester) {
      return res.json({
        status: 400,
        message: 'Requester is not defined.'
      });
    }

    if (network !== 'skale' && network !== 'skaleTestnet') {
      if (requester) {
        cache.del(requester);
      }

      return res.json({
        status: 400,
        message: 'Invalid network requested.'
      });
    }

    const provider = providerManager.getProvider(network);

    const addressBalance = await provider.getBalance(requester);
    const maxClaimThreshold = process.env.FAUCET_MAX_BALANCE_WEI;

    if (addressBalance.gt(maxClaimThreshold)) {
      cache.del(requester);

      return res.json({
        status: 400,
        message: 'Address balance is too high.'
      });
    }

    const walletSigner = walletManager
      .getWallet('skale', network)
      .connect(provider);

    const result = await walletSigner.sendTransaction({
      to: requester,
      value: ethers.utils.parseUnits(
        process.env.FAUCET_DISTRIBUTION_AMOUNT_WEI,
        'wei'
      )
    });

    debug('SKALE faucet dispatched: ', result);

    if (!result || !result['hash']) {
      console.error('Faucet tx failed', result);

      cache.del(requester);

      return res.json({
        status: 500,
        data: 'Transaction failed.'
      });
    }

    return res.json({
      status: 200,
      data: result['hash']
    });
  } catch (e) {
    console.error(e);
    cache.del(requester);
    return res.json({
      status: 500,
      data: 'An unknown error has occurred.'
    });
  }
});

export { router };
